from casadi import *
from acados_template import AcadosModel

def bicycle_model():
    model_name = "BicycleModelWarmStart"


    L = 0.65

    x1 = SX.sym('a')
    x2 = SX.sym('v')
    x3 = SX.sym('s')

    x = vertcat(x1, x2, x3)

    u1 = SX.sym('da')
    u2 = SX.sym('dTs')
    u = vertcat(u1, u2)

    p = []

    dX_1 = SX.sym('dx1')
    dX_2 = SX.sym('dx2')
    dX_3 = SX.sym('dx3')
    dx = vertcat(dX_1, dX_2, dX_3)

    fnc = u[1]*vertcat(
        u[0],
        x[0],
        x[1]
    )
    model = AcadosModel()
    model.f_expl_expr = fnc
    model.f_impl_expr = dx - fnc
    model.x = x
    model.u = u
    model.p = p
    model.xdot = dx
    model.name = model_name
    return model
    

#[jerk, dT]

#dA = control[0] * dT
#dV = a * dT
#dS = v * dT