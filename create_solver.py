from casadi import *
from acados_template import AcadosOcp, AcadosOcpSolver


from dynamics import bicycle_model
import numpy as np

import matplotlib.pyplot as plt

from interpolation import *

N_HORIZON = 20

S = 22.59

V_s = 0.0
V_f = 0.0

A_lim = 0.4
dA_lim = 0.5

# T_modeling = 2.*abs((V_s  - V_f) / A_lim)

T_modeling = 60

def create_solver(time_horizon, stage_num):
    constr_x_lower = np.array([0]*3)
    constr_x_upper = np.array([0]*3)
    constr_u_lower = np.array([0]*2)
    constr_u_upper = np.array([0]*2)

    ocp = AcadosOcp()
    model = bicycle_model()
    ocp.model = model

    Nx = 3
    Nu = 2
    ocp.dims.np = 0
    ocp.dims.nu = Nu
    ocp.dims.nx = Nx
    ocp.dims.N = stage_num
    ocp.dims.nbx = Nx
    
    ocp.cost.cost_type = 'EXTERNAL'
    ocp.cost.cost_type_e = 'EXTERNAL'

    ocp.model.cost_expr_ext_cost = 10000*model.u[1]
    ocp.model.cost_expr_ext_cost_e = 0

    ocp.constraints.idxbx_e = np.array(range(3))
    ocp.constraints.lbx_e = np.array([0]*3)
    ocp.constraints.ubx_e = np.array([0]*3)
    ocp.constraints.x0 = np.array([0.0]*3)
    ocp.constraints.Jbx = np.eye(Nx,Nx)
    ocp.constraints.lbx = constr_x_lower
    ocp.constraints.ubx = constr_x_upper


    # ocp.model.con_h_expr = vertcat(
    #     model.x[1]
    # )

    # ocp.constraints.lh = np.array([0])
    # ocp.constraints.uh = np.array([2])


    ocp.constraints.idxbu = np.array(range(2))
    ocp.constraints.lbu = constr_u_lower
    ocp.constraints.ubu = constr_u_upper

    ocp.solver_options.tf = time_horizon
    ocp.solver_options.qp_solver = 'FULL_CONDENSING_HPIPM'
    ocp.solver_options.regularize_method = 'PROJECT'
    ocp.solver_options.globalization = 'FIXED_STEP'
    ocp.solver_options.nlp_solver_step_length = 0.3
    ocp.solver_options.alpha_reduction = 0.7
    ocp.solver_options.alpha_min = 0.01
    ocp.solver_options.hessian_approx = 'GAUSS_NEWTON'
    ocp.solver_options.integrator_type = 'ERK'
    ocp.solver_options.nlp_solver_type = 'SQP'
    ocp.solver_options.levenberg_marquardt = 110.1
    ocp.solver_options.nlp_solver_max_iter = 10000
    ocp.solver_options.qp_solver_iter_max = 10000
    ocp.solver_options.print_level = 0
    ocp.solver_options.qp_tol = 0.001
    ocp.solver_options.tol = 0.001
    ocp.solver_options.qp_solver_tol_stat = 0.5
    ocp.solver_options.nlp_solver_tol_stat = 0.7
    
    t_ocp = AcadosOcpSolver(ocp)

    return t_ocp


def set_boundaries(solver,  N_stages, scaling_factor, conditions):
    V_start = conditions[0]
    V_finish = conditions[1]
    A_limit = conditions[2]
    dA_limit = conditions[3]
    S = conditions[4]

    for i in range(N_stages):
        solver.set(i, 'lbu', np.array([-dA_limit, 1.0]))
        solver.set(i, 'ubu', np.array([dA_limit, 1000]))
        
    for i in range(N_stages+1):
        if (V_start > V_finish):
            solver.set(i, 'lbx', np.array([-A_limit, 0.0, 0]))
            solver.set(i, 'ubx', np.array([A_limit, 2.0, S]))
        else:
            solver.set(i, 'lbx', np.array([-A_limit, 0.0, 0]))
            solver.set(i, 'ubx', np.array([A_limit, 2.0, S]))

    solver.set(0, 'lbx', np.array([0, V_start, 0]))
    solver.set(0, 'ubx', np.array([0, V_start, 0]))

    solver.set(N_stages, 'lbx', np.array([0, V_finish, S]))
    solver.set(N_stages, 'ubx', np.array([0, V_finish, S]))

    vel_dec = (V_start - V_finish) / (N_stages)
    s_dec = S / (N_stages )

    for i in range(N_stages+1):
        solver.set(i, 'x', np.array([0, V_start - vel_dec*i, i*s_dec]))
    for i in range(N_stages):
        solver.set(i, 'u', np.array([0, scaling_factor]))


def retrieve_solution(solver, T_modeling, N_stages):
    solution = {
        'control': [],
        'state': [],
        'timeline': []
    }
    for i in range(N_HORIZON + 1):
        tmp_list = []
        tmp_list.append(solver.get(i, 'x')[0])
        tmp_list.append(solver.get(i, 'x')[1])
        tmp_list.append(solver.get(i, 'x')[2])
        solution['state'].append(tmp_list)

    for i in range(N_HORIZON):
        tmp_list = []
        tmp_list.append(solver.get(i, 'u')[0])
        tmp_list.append(solver.get(i, 'u')[1])
        solution['control'].append(tmp_list)

    time_line = [0]
    stage_dt = T_modeling / (N_stages)
    for i in solution['control']:
        time_line.append(time_line[-1] + i[1]*stage_dt)

    solution['timeline'] = time_line
    solution['control'] = solution['control']
    return solution


def plot_solution(solution, title, marker):

    a = [i[0] for i in solution['state']]
    v = [i[1] for i in solution['state']]
    s = [i[2] for i in solution['state']]

    da = [i[0] for i in solution['control']]
    dt = [i[1] for i in solution['control']]

    x_ax = solution['timeline']

    fig, ax = plt.subplots(2,2)
    fig.suptitle(title)
    plt.grid(True)
    ax[0][0].plot(x_ax, a, marker)
    ax[0][1].plot(x_ax, v, marker)
    ax[1][0].plot(x_ax, s, marker)
    ax[1][1].step(x_ax[:len(x_ax) - 1], dt, marker)

    ax[0][0].set_title('acceleration')
    ax[0][1].set_title('speed')
    ax[1][0].set_title('distance')
    ax[1][1].set_title('ScaleFactor')

    ax[0][0].grid(True)
    ax[0][1].grid(True)
    ax[1][0].grid(True)
    ax[1][1].grid(True)

    plt.show()


if __name__ == '__main__':
    solver = create_solver(4., N_HORIZON)
    set_boundaries(solver, N_HORIZON, T_modeling, [V_s, V_f, A_lim, dA_lim, S])
    status = solver.solve()
    solver.print_statistics()
    solution = retrieve_solution(solver, 4., N_HORIZON)
    print(f"V_start {solution['state'][0][1]}")
    print(f"V_end {solution['state'][-1][1]}")
    new_sol = interpolate(125, solution)
    plot_solution(new_sol, f'Distance {S}, Tf {T_modeling}', '-')
    plot_solution(solution, f'Distance {S}, Tf {T_modeling}', 'o')