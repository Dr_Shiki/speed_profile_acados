

import copy


def get_in_point(state, control, t):
    a0 = state[0]
    v0 = state[1]
    s0 = state[2]
    da = control[0]
    return [
        a0 + da*t,
        a0*t+0.5*da*t*t+v0,
        0.5*a0*t*t + (1/6)*da*t*t*t + s0 + v0*t
    ]


def interpolate(pt_num, sol):

    t_all = sol['timeline'][-1]
    new_states = [sol['state'][0]]
    new_timeline = [0]
    new_control = [sol['control'][0]]
    t_step = t_all / pt_num
    cur_t = t_step
    cur_pt_index = 0


    while cur_t < t_all:
        if (cur_t > sol['timeline'][cur_pt_index+1]):
            cur_pt_index+=1
            dt = cur_t - sol['timeline'][cur_pt_index]
            new_states.append(get_in_point(
                sol['state'][cur_pt_index],
                sol['control'][cur_pt_index],
                dt
            ))
        else:
            dt = cur_t - new_timeline[-1]
            new_states.append(get_in_point(
                new_states[-1],
                new_control[-1],
                cur_t - new_timeline[-1]
            ))
        new_control.append(sol['control'][cur_pt_index])
        new_timeline.append(cur_t)
        cur_t += t_step

    new_sol = copy.copy(sol)
    new_sol['state'] = new_states
    new_sol['control'] = new_control[:len(new_control)-1]
    new_sol['timeline'] = new_timeline
    return new_sol